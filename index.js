// Setup basic express server
const express = require('express');

const app = express();
const server = require('http').createServer(app);
const io = require('socket.io')(server);

const port = process.env.PORT || 3000;

server.listen(port, function () {
  console.log('Server listening at port %d', port);
});

// Routing
app.use(express.static(__dirname + '/public'));

let usersOnline = [];
const colors = ['#544949', '#e24d4d', '#a070a5', '#ca22dc', '#223fdc', '#222f73', '#1ed4ad', '#1b7360', '#9fd820', '#718e30'];
let countColors = 0;
const colorsForUsers = {};

io.on('connection', function (socket) {
  let name;
  let soketColor;
  if (socket.handshake.query.username === 'null') {
    name = (socket.id).toString().substr(0, 4);
    soketColor = colors[countColors];
    Object.assign(colorsForUsers, { [name]: soketColor });
  } else {
    name = socket.handshake.query.username;
    soketColor = colorsForUsers[name];
  }
  (countColors >= 9) ? countColors = 0 : countColors++;
  usersOnline.push(name);
  socket.emit('userName', name, usersOnline);
  socket.broadcast.emit('newUser', name);
  socket.on('disconnect', function() {
    usersOnline = usersOnline.filter(item => item !== name);
    socket.broadcast.emit('userOffline', name);
  });
  socket.on('canvasMousedown', function (data) {
    socket.broadcast.emit('canvasMousedown', data);
  });
  socket.on('canvasMousemove', function (data) {
    socket.broadcast.emit('canvasMousemove', data);
  });
  socket.on('canvasMouseup', function (data) {
    socket.broadcast.emit('canvasMouseup', data);
  });
  socket.on('message', function (message) {
    socket.broadcast.emit('message', name, message, soketColor);
    socket.emit('message', name, message, soketColor);
  });
});
