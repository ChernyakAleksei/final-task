import io from 'socket.io-client';
import 'font-awesome/less/font-awesome.less';
import './style.css';
import innerHTML from './canvas';
import Chat from './chat.js';

const $ = require('jquery');

const usernameFromStorag = sessionStorage.getItem('userNameForPaint');
const socket = io(window.location.origin, { query: `username=${usernameFromStorag}` });

const wraperCanvas = $('.wraper-canvas').html(innerHTML);

const context = document.getElementById('canvas').getContext('2d');
const canvas = $('#canvas');

class PaintV02 {
  constructor(context, $canvas) {
    this.canvasWidth = 1000;
    this.canvasHeight = 600;
    this.clickColor = '#000';
    this.paint = false;
    this.curTool = 'marker';
    this.canvas = $canvas;
    this.context = context;
    this.lastEvent = {};
    this.lineType = 'round';
    this.canvas.attr('width', this.canvasWidth);
    this.canvas.attr('height', this.canvasHeight);
    this.init();
  }
  init() {
    this.controlEventListners();
    this.canvas.mousedown((e) => {
      this.paint = true;
      if (this.curTool === 'eraser') {
        this.clickColor = '#fff';
      } else {
        this.clickColor = $('input[type=color]').val();
      }
      this.lastEvent = {
        eoffsetX: e.offsetX,
        eoffsetY: e.offsetY,
        clickColor: this.clickColor,
        clickSize: +$('input[type=range]').val(),
        lineType: this.lineType
      };
      this.eventNew = {
        lastEventOffsetX: this.lastEvent.eoffsetX,
        lastEventOffsetY: this.lastEvent.eoffsetY,
        eoffsetX: e.offsetX,
        eoffsetY: e.offsetY
      };
      this.eventPoints = Object.assign(this.lastEvent, this.eventNew);
      socket.emit('canvasMousedown', this.eventPoints);
      this.draw(this.eventPoints);
    });
    this.canvas.mousemove((e) => {
      if (this.paint) {
        this.eventNew = {
          lastEventOffsetX: this.lastEvent.eoffsetX,
          lastEventOffsetY: this.lastEvent.eoffsetY,
          eoffsetX: e.offsetX,
          eoffsetY: e.offsetY,
        };
        this.eventPoints = Object.assign(this.lastEvent, this.eventNew);
        socket.emit('canvasMousemove', this.eventPoints);
        this.draw(this.eventPoints);
        this.lastEvent = Object.assign({}, this.eventPoints);
      }
    });
    this.canvas.mouseup(() => {
      this.paint = false;
      socket.emit('canvasMouseup', false);
    });
    this.canvas.mouseleave(() => {
      this.canvas.mouseup();
    });
  }

  draw({ lastEventOffsetX, lastEventOffsetY, eoffsetX, eoffsetY, clickColor, clickSize, lineType }) {
    this.context.beginPath();
    this.context.moveTo(lastEventOffsetX, lastEventOffsetY);
    this.context.lineTo(eoffsetX, eoffsetY);
    this.context.strokeStyle = clickColor;
    this.context.lineWidth = clickSize;
    this.context.lineCap = lineType;
    this.context.stroke();
  }

  clearCanvas() {
    context.clearRect(0, 0, this.canvasWidth, this.canvasHeight);
  }
  controlEventListners() {
    $('.clearCanvas').click(() => { this.clearCanvas(); });
    $('.type-lines').click((e) => {
      $(e.target).addClass('active');
      $(e.target).siblings().removeClass('active');
      this.lineType = e.target.value;
    });
    $('.tools').click((e) => {
      $(e.target).addClass('active');
      $(e.target).siblings().removeClass('active');
      this.curTool = e.target.value;
      if (this.curTool === 'eraser') {
        this.canvas.css({ cursor: 'url(./img/eraser.png) 0 40, auto' });
      } else {
        this.canvas.css({ cursor: 'url(./img/br.png) 0 40, auto' });
      }
    });
    $('.fa-download').click(() => {
      const canv = document.getElementById('canvas');
      window.location = canv.toDataURL('image/png');
    });
  }
}

const paint02 = new PaintV02(context, canvas);

socket.on('canvasMousedown', (data) => { paint02.draw(data); });
socket.on('canvasMousemove', (data) => { paint02.draw(data); });
socket.on('canvasMouseup', () => { paint02.paint = false; });

const chat = new Chat();

socket.on('userName', (name, users) => {
  chat.setName = name;
  sessionStorage.setItem('userNameForPaint', name);
  chat.addUsers(users);
});
socket.on('newUser', (name) => {
  chat.addUsers(name);
});

socket.on('userOffline', (name) => {
  chat.removeUser(name);
});
socket.emit('connection', sessionStorage.getItem('userNameForPaint'));

$('#inputChat').on('keydown', (e) => {
  if (e.which === 13) {
    const message = $(e.target).val();
    socket.emit('message', message);
    $('#inputChat').val(null);
  }
});
socket.on('message', (name, message, color) => {
  chat.addMessage(name, message, color);
});
