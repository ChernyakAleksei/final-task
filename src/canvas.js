 export default `
    <aside>
      <div class="controls">
        <input type="color" />
        <input type="range" min="1" max="60" />
      </div>
      <button class="clearCanvas fa fa-undo"></button>
      <div class="tools">
        <button value='marker' class="fa fa-paint-brush active"></button>
        <button value='eraser' class="fa fa-eraser"></button>
      </div>
      <div class="type-lines">
        <button value='round' class="fa fa-google-wallet active"></button>
        <button value='butt' class="fa fa-stack-overflow"></button>
      </div>
      <button class="fa fa-download"></button>
    </aside>
    <div class="canvas">
      <canvas id="canvas"></canvas>
    </div>
    <div class="onlineUsers">
      <p>Users online<p>
      <div class='list-onlineUsers'></div>
    </div>
    <div class = "chat-wraper">
      <div  class="chat">
        <ul id="chat" class="item-messages"></ul>
      </div>
      <input type="text" id="inputChat" class = "inputChat" autofocus>
    </div>
    `;
