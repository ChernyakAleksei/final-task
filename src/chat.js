
const $ = require('jquery');

export default class Chat {
  constructor() {
    this.usersStore = [];
  }
  addUsers(users) {
    if (Array.isArray(users)) {
      users.forEach((item) => {
        this.usersStore.push(item);
      });
    } else {
      this.usersStore.push(users);
    }
    this.rewriteUsers();
  }
  rewriteUsers() {
    const documentFragment = $(document.createDocumentFragment());
    for (let i = 0; i < this.usersStore.length; i++) {
      const $p = $('<p></p>').text(this.usersStore[i]);
      if (this.usersStore[i] === this.Name) {
        $p.css('color', '#1ae810');
      }
      documentFragment.append($p);
    }
    $('.list-onlineUsers').html('');
    $('.list-onlineUsers').append(documentFragment);
  }
  removeUser(name) {
    this.usersStore = this.usersStore.filter(item => item !== name);
    this.rewriteUsers();
  }
  addMessage(name, message, color) {
    const documentFragment = $(document.createDocumentFragment());
    let $NameUser;
    if (name === this.Name) {
      $NameUser = $('<li></li>').text(name).css('color', '#1ae810');
    } else {
      $NameUser = $('<li></li>').text(name).css('color', color);
    }
    const $liMessage = $('<li></li>').text(message);
    documentFragment.append($NameUser).append($liMessage);
    $('#chat').append(documentFragment);
    $('#chat').animate({ scrollTop: $('#chat').prop('scrollHeight') });
  }
  set setName(newName) {
    this.Name = newName;
  }
}
